SUMMARY = "My Custom Image"
DESCRIPTION = "This is my customized image containing a simple hello-world"
 
inherit core-image

IMAGE_INSTALL += " \
	mc \
	openssh \
	python3	\
	hello-world \
"
 
export IMAGE_BASENAME = "hello-image"
